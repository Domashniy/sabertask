/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vasya
 *
 * Created on December 14, 2017, 9:00 PM
 */

#include <list>
#include <bitset>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <string.h>

#include "list/List.h"

using namespace std;
using saber::List;

void printBin(int t) {
    list<int> b;
    for (int c = 0; c < 64; c++) {
        t & 1 == 1 ? b.push_front(1) : b.push_front(0);
        t = t >> 1;
        if (t == 1) {
            b.push_front(1);
            break;
        }
    }
    for (auto i : b) {
        cout << i ;
    }
    cout << endl;
}
void RemoveDups (char *pStr) {
    char pc = pStr[0];
    vector<char> str;
    str.push_back(pc);
    size_t len = strlen(pStr);
    for (int i = 0; i < len; i++) {
        char c = pStr[i];
        if (c != pc) {
            str.push_back(c);
            pc = c;
        }
    }
     for (auto i : str) {
        cout << i ;
    }
    cout << endl;
}
int main(int argc, char** argv) {

    cout << "Print signed integer number in binary representation" << endl;
    cout << endl;

    int a = -9;
    cout << "My  function" << endl;
    printBin(a);
    cout << "STL function" << endl;
    bitset<64> bs(a);
    cout << bs << endl;
    cout << endl;
    
    cout << "Remove duplicates from string" << endl;
    cout << endl;
    
    char str[] = "aaaaaaa bbb aaa";
    cout << "Original string: " << endl;
    cout << string(str) << endl;

    cout << "Fixed string: " << endl;
    RemoveDups(str);
    cout << endl;
    
    cout << "List serialization and deserialization" << endl;
    cout << endl;

    FILE* fileOut = fopen("structs.dat", "wb");
    
    List list1;
    list1.pushBack("hei");
    list1.pushBack("how");
    list1.pushBack("rock");
    list1.pushBack("");
    list1.pushBack("roll");
    
    list1.generateRandomLinks();
    cout << list1;
    list1.Serialize(fileOut);
    
    fclose(fileOut);
    
    List list2;
    
    FILE* fileIn = fopen("structs.dat", "rb");
    list2.Deserialize(fileIn);
    cout << list2;
    fclose(fileIn);
    cout << endl;
    
    cout << "Hope for an interview" << endl;

    return 0;
}

