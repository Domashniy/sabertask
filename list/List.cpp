/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   List.cpp
 * Author: vasya
 * 
 * Created on December 14, 2017, 9:50 PM
 */

#include "List.h"

#include <map>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <algorithm>

using std::endl;
using std::cout;
using std::vector;
using std::map;
using std::string;
using namespace saber;

using ByteArray = vector<char>;

#pragma pack(push, 1)

/**
 * Header of list elements for memory mapping
 */
struct BlockHeader {

    BlockHeader() {
    }

    BlockHeader(int randNum, int dataBytes) :
    randNum(randNum),
    dataBytes(dataBytes) {
    }

    int randNum; // position of rand node for current node regarding 
    // to the begin of the list
    int dataBytes; // amount of bytes for data storage
};

/**
 * Additional struct for list nodes representation as vector elements and future
 * serialization and deserialization
 */
struct FlatNode : BlockHeader {

    FlatNode() {
    }

    FlatNode(int randNum, int dataBytes, string data) :
    BlockHeader(randNum, dataBytes),
    data(data) {
    }

    string data; // data of ListNode
};
#pragma pack(pop)

/**
 * Convert FlatNode to vector<char>
 * @param node FlatNode element
 * @return vector<char>
 */
ByteArray convertFlatNodeToBytes(const FlatNode& node) {
    
    int dataBytes = sizeof (char)*node.data.length();
    const int bytesAmount = sizeof (BlockHeader) + dataBytes;

    ByteArray bytesArray;
    bytesArray.resize(bytesAmount);

    memcpy(bytesArray.data(), (BlockHeader*) &(node), sizeof (BlockHeader));
    memcpy(bytesArray.data() + sizeof (BlockHeader),
            (node.data.c_str()), node.dataBytes);

    return bytesArray;
}

void printError(string message) {
    
    cout << "Error occurred: " << message << endl;
}

ListNode createUnlinkedNode(string data) {
    
    return ListNode{nullptr, nullptr, nullptr, data};
}

List::List() :
head(nullptr),
tail(nullptr),
count(0) {
}

List::~List() {
    
    for (ListNode* next = head; next; next = next->next) {
        delete next;
    }

}

/**
 * Serialization of list class
 * @param file file pointer
 */
void List::Serialize(FILE* file) {

    if (file == 0) {
        printError("empty file ptr");
        return;
    }

    //rand node ptr <-> sequence number of node whose randPtr points to it
    map<ListNode*, int> randsMap;
    vector<FlatNode> flatNodesVec;
    flatNodesVec.reserve(count);
    int curNum = 0;
    for (ListNode* next = head; next; next = next->next) {
        flatNodesVec.emplace_back(-1, next->data.size(), next->data);
        randsMap[next->rand] = curNum;
        curNum++;
    }

    curNum = 0;
    for (ListNode* next = head; next; next = next->next) {
        if (randsMap.find(next) != randsMap.end()) {
            int hostNum = randsMap[next];
            flatNodesVec[hostNum].randNum = curNum;
        }
        curNum++;
    }

    for (auto i : flatNodesVec) {
        ByteArray bytesArray = convertFlatNodeToBytes(i);
        fwrite(bytesArray.data(), bytesArray.size(), 1, file);
    }

    fflush(file);
}

/**
 * Deserialization of list class
 * @param file file pointer
 */
void List::Deserialize(FILE* file) {

    if (file == 0) {
        printError("empty file ptr");
        return;
    }

    fseek(file, 0, SEEK_END);
    const int fileSize = ftell(file);
    rewind(file);
    int bytesReadOverFile = 0;

    //sequence number of rand node <-> node who points to this rand
    map<int, ListNode*> randsMap;

    int counter = 0;
    while (bytesReadOverFile < fileSize) {
        BlockHeader header;

        int bytesRead = fread(&header, 1, sizeof (header), file);
        if (bytesRead != sizeof (header)) {
            printError("file error, unable to read header");
            return;
        }

        bytesReadOverFile += bytesRead;

        ByteArray chars(header.dataBytes);
        bytesRead = fread(chars.data(), 1, header.dataBytes, file);
        if (bytesRead != header.dataBytes) {
            printError("file error, unable to read data");
            return;
        }

        bytesReadOverFile += bytesRead;

        string data = string(chars.data(), header.dataBytes);

        pushBack(createUnlinkedNode(data));
        if (header.randNum == -1) {
            tail->rand = nullptr;
        } else {
            randsMap[header.randNum] = tail;
        }
        counter++;
    }

    counter = 0;
    for (ListNode* next = head; next; next = next->next) {
        if (randsMap.find(counter) != randsMap.end()) {
            randsMap[counter]->rand = next;
        }
        counter++;
    }
}

/**
 * Add ListNode object to the end of the list
 * @param node element to add
 */
void List::pushBack(const ListNode& node) {
    
    ListNode* newNode = new ListNode{node.prev, node.next,
        node.rand, node.data};
    if (!head) {
        head = tail = newNode;
    } else {
        newNode->prev = tail;
        tail->next = newNode;
        tail = newNode;
    }
    count++;
}

/**
 * Add ListNode object initialized by data to the end of the list
 * @param data data for the element
 */
void List::pushBack(std::string data) {
    
    pushBack(createUnlinkedNode(data));
}

/**
 * Fill in list elements with links to random nodes of this list
 */
void List::generateRandomLinks() {
    
    vector<ListNode*> randLinksVec;
    randLinksVec.reserve(count);
    for (ListNode* next = head; next; next = next->next) {
        randLinksVec.push_back(next);
    }
    std::random_shuffle(randLinksVec.begin(), randLinksVec.end());

    int i = 0;
    for (ListNode* next = head; next; next = next->next) {
        next->rand = randLinksVec[i++];
    }
}

namespace saber {

    /**
     * Overloaded operator for list printing
     * @param stream reference to stream
     * @param list list for printing
     * @return stream
     */
    std::ostream& operator<<(std::ostream& stream, const List& list) {
        
        cout << "-------List print begin------" << endl;
        cout << "Count: " << list.count << endl;
        for (ListNode* next = list.head; next; next = next->next) {
            cout << "data: " << next->data << " rand data: "
                    << (next->rand == 0 ? "NULL" : next->rand->data) << endl;
        }
        cout << "------------end--------------" << endl;

    }
}