/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   List.h
 * Author: vasya
 *
 * Created on December 14, 2017, 9:50 PM
 */

#ifndef LIST_H
#define LIST_H

#include <string>

namespace saber {
struct ListNode {
    ListNode * prev;
    ListNode * next;
    ListNode * rand; // указатель на произвольный элемент данного списка либо NULL
    std::string data;
};

class List {
public:
    List();
    virtual ~List();

    void Serialize(FILE * file);
    void Deserialize(FILE * file);
    
    void pushBack(const ListNode& node);
    void pushBack(std::string data);
    
    void generateRandomLinks();

    friend std::ostream& operator<< (std::ostream& stream, const List& list);

private:
    ListNode * head;
    ListNode * tail;
    int count;
    
};
}

#endif /* LIST_H */

